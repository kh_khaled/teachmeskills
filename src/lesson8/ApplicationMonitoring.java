package lesson8;

public class ApplicationMonitoring {
    public static void main(String[] args) {
        MonitoringSystem monitoringSystem = new MonitoringSystem() {
            @Override
            public void startMonitoring(){
                System.out.println("Monitoring has been starting");
            }
        };
        monitoringSystem.startMonitoring();
    }
}
