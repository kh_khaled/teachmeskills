package lesson2;

import java.util.Random;
import java.util.Scanner;

public class FirstClass {
    public static void main(String[] args) {

        countPositiveInt();
        countPositiveAndNegativeInt();

        int[] arr = new int[10];

        System.out.print("\nМассив чисел: ");
        for (int index = 0; index < arr.length; index++) {
            arr[index] = randomInt();
            System.out.print(arr[index] + " ");
        }
        minimum(arr);
        countProgrammer();
    }

    private static void countPositiveInt() {
        int a = -2, b = 3, c = -4, counterPos = 0;

        if (a > 0) counterPos++;
        if (b > 0) counterPos++;
        if (c > 0) counterPos++;

        System.out.println("\nДаны три целых числа: " + a +", "+ b +", "+ c);
        System.out.println("Количество положительных чисел: " + counterPos);
    }

    private static void minimum(int[] arr) {
        int minValue = arr[0];

        for (int index = 1; index < arr.length; index++) {
            if (arr[index] < minValue) {
                minValue = arr[index];
            }
        }
        System.out.println("\nМинимум в массиве: " + minValue);
    }

    private static void countPositiveAndNegativeInt() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nВведите 3 целых числа (включая отрицательные)");

        int[] inputNumbers = new int[3];
        int minValue = inputNumbers[0], maxValue = inputNumbers[0];
        int counterPos = 0, counterNeg = 0;

        for (int index = 0; index < inputNumbers.length; index++) {
            inputNumbers[index] = scanner.nextInt();

            if (inputNumbers[index] < minValue) {
                counterNeg++;
            }
            if (inputNumbers[index] > maxValue) {
                counterPos++;
            }
        }
        System.out.println("Отрицательных чисел: " + counterNeg);
        System.out.println("Положительных чисел: " + counterPos);
    }

    private static void countProgrammer() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nВведите количество программистов: ");
        int inputNum = scanner.nextInt();
        String str = "программист", end1 = "ов", end2 = "а";

        if ((inputNum >= 5 && inputNum <= 20) || (inputNum == 0) || (inputNum > 20 && (inputNum % 10 >=5 && inputNum % 10 <= 9) || inputNum % 10 == 0) ||
                (inputNum % 100 > 10 && inputNum % 100 < 20)) {
            System.out.println("В зале сидит " + inputNum + " " + str + end1);
        }
        if ((inputNum % 10 == 1) && (inputNum % 100 != 11)) {
            System.out.println("В зале сидит " + inputNum + " " + str);
        }
        if ((inputNum % 10 >= 2) && (inputNum % 10 <= 4)) {
            System.out.println("В зале сидит " + inputNum + " " + str + end2);
        }

    }
    private static int randomInt() {
        Random random = new Random();
        return random.nextInt(100);
    }
}
