package lesson7;

import java.util.Date;

public class AnimalService {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setAnimalID(1);
        cat.setName("Barsik");
        Date data = new Date();
        cat.setData(data);
        cat.setEyesColor("Green");

        Dog dog = new Dog();
        dog.setWeight(56.23);
        dog.setAnimalID(2);
        dog.setName("Sharik");
        dog.setData(data);

        Tiger tiger = new Tiger();
        tiger.setAnimalID(3);
        tiger.setName("Tigrik");
        tiger.setData(data);
        tiger.setCountEatenEmployees(489);

        callPrint(cat);
        callPrint(dog);
        callPrint(tiger);
    }

    private static void callPrint (Animal animal){
        animal.printInfo();
    }
}
