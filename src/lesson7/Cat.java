package lesson7;

import java.util.Date;

public class Cat extends Animal {
    public String eyesColor;

    public Cat() {
    }

    public Cat(String name, Date data, String eyesColor) {
        super(name, data);
        this.eyesColor = eyesColor;
    }

    public void setEyesColor(String eyesColor){
        this.eyesColor = eyesColor;
    }

    public void printInfo() {
        System.out.println(animalID + " " + name + " " + data + " " + eyesColor);
    }
}
