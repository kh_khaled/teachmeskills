package lesson7;

import java.util.Date;

public class Tiger extends Cat {
    int countEatenEmployees;

    public Tiger() {
    }

    public Tiger(String name, Date data, String eyesColor, int countEatenEmployees) {
        super(name, data, eyesColor);
        this.countEatenEmployees = countEatenEmployees;
    }

    public int getCountEatenEmployees() {
        return countEatenEmployees;
    }

    public void setCountEatenEmployees(int countEatenEmployees) {
        this.countEatenEmployees = countEatenEmployees;
    }

    public void printInfo() {
        System.out.println(animalID + " " + name + " " + data + " " + countEatenEmployees);
    }
}
