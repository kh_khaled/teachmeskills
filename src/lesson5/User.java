package lesson5;

public class User {
    String name, login;
    int id;

    public void setId(int id) {
        this.id = id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "Имя: " + name + " / Логин: " + login;
    }

}
