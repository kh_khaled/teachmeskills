package lesson5;

import java.util.Random;

public class UserCalc {
    public static void main(String[] args) {


        String[] names = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа"};
        String[] logins = new String[]{"SashaLogin", "PashaLogin", "DashaLogin", "JulyLogin", "NatalyLogin"};
        User[] users = new User[5];

        for (int i = 0; i < users.length; i++) {
            User user = new User();
            user.setName(names[i]);
            user.setLogin(logins[i]);
            users[i] = user;
        }

        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i].toString());
        }
    }


    private static int getRandom(int maxLimit) {
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}
