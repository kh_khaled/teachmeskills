package lesson6;

public class Student {
    String name;
    int group, grade;

    /*Student (String name, int group, int grade) {
        this.name = name;
        this.group = group;
        this.grade = grade;
    }*/

    public void setName(String name) {
        this.name = name;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
    public boolean thirdGroup() {
        return group == 3;
    }

    @Override
    public String toString() {
        return "\nИмя: " + name + "\nГруппа: " + group + "\nОценка: " + grade;
    }


}
