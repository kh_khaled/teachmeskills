package lesson6;

public class Cat {
    String name;
    int age, gram;

    Cat(String name, int age, int gram) {
        this.name = name;
        this.age = age;
        this.gram = gram;
    }

    public boolean fullness() {
        return gram > 100;
    }
    @Override
    public String toString() {
        return "Имя: " + name + "\nВозраст: " + age + "\nКол-во корма: " + gram;
    }
}
