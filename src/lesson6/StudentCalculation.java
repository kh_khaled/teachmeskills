package lesson6;

import java.util.Random;

public class StudentCalculation {
    public static void main (String[] args) {
        String[] name = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа"};
        Student[] students = new Student[10];
        for (int i = 0; i < students.length; i++) {
            Student student = new Student();
            student.setGrade(getRandom(5));
            student.setName(name[getRandom(4)]);
            student.setGroup(getRandom(4));
            students[i] = student;

            if (student.thirdGroup()) {
                System.out.println(students[i].toString());
            }
        }
    }

    private static int getRandom(int maxLimit){
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}
