package lesson8Homework2;

public class Rectangle extends Shape {
    private static final String name = "прямоугольника";

    private double width;
    private double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double getArea() {
        return width * height;
    }

    @Override
    public String getName() {
        return name;
    }
}
