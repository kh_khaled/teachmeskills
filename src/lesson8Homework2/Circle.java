package lesson8Homework2;

public class Circle extends Shape {
    private static final String name = "круга";

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        double area = Math.PI * radius * radius;
        return area;
    }

    @Override
    public String getName() {
        return name;
    }
}
