package lesson8Homework2;

public class ShapeApplication {
    public static void main(String[] args) {
        Shape[] shape = {
                new Rectangle(10, 12),
                new Circle(13)
        };

        for(Shape shp : shape)
            System.out.println("Площадь " + shp.getName() + " = " + shp.getArea());
    }
}
