package lesson8Homework2;

public abstract class Shape {
    public abstract double getArea();
    public abstract String getName();
}
