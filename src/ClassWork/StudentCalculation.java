package ClassWork;

import java.util.Random;

public class StudentCalculation {
    public static void main (String[] args) {
        String[] name = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа"};
        Student[] students = new Student[14];
        for (int i = 0; i < students.length; i++) {
            Student student = new Student();
            student.setGrade(getRandom(5));
            student.setName(name[getRandom(4)]);
            student.setGroup(getRandom(3));
            students[i] = student;
        }

        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }
    }

    private static int getRandom(int maxLimit){
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}
