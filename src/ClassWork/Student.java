package ClassWork;

public class Student {
    String name;
    int group, grade;

    public void setName(String name) {
        this.name = name;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Имя: " + name;
    }
}
