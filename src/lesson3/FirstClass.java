package lesson3;

public class FirstClass {
    public static void main(String[] args) {

        sortMax();
        System.out.println();
        sortMin();
        System.out.println();
        stupidSort();
        System.out.println();
        minOfMatrix();
    }

    private static void sortMax() {
        int[] arr = new int[]{5, 2, 8, 1, 4};

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        System.out.println("Сортировка по возрастанию: ");
        for (int a : arr) {
            System.out.print(a + " ");
        }
    }

    private static void sortMin() {
        int[] arr = new int[]{5, 2, 8, 1, 4};

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] < arr[j + 1]) {
                    int tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        System.out.println("Сортировка по убыванию: ");
        for (int a : arr) {
            System.out.print(a + " ");
        }
    }

    private static void stupidSort() {
        int[] arr = new int[]{5, 2, 8, 1, 4};

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                    j = 0;
                } else
                    j += 1;
            }
        }
        System.out.println("Глупая сортировка по возрастанию: ");
        for (int a : arr) {
            System.out.print(a + " ");
        }
        System.out.println();
    }

    private static void minOfMatrix() {
        int[][] arr = new int[][] {
                {5, 2, 8, 3, 4},
                {7, 5, 2, 9, 6},
                {1, 9, 7, 8, 5}
        };

        int minArrNew = arr[0][0];

        for (int i = 0; i < arr.length; i++) {
            int minValue = arr[i][0];
            for (int j = 1; j < arr[i].length; j++) {
                if (arr[i][j] < minValue)
                    minValue = arr[i][j];
            }
            System.out.println("Минимум в строке: " + minValue);

            if (minValue < minArrNew) {
                minArrNew = minValue;
            }
        }
        System.out.println("Минимум в двумерном массиве: " + minArrNew);
    }
}
