public class FirstClass {
    public static void main(String[] args) {

        int a = 7, b = 4;
        double q = 3.33, w = 4.45;

        // Задание 1. Даны 2 числа, вывести меньшее из них
        System.out.println("Наши заданные 2 целых числа: " + a + " и " + b);

        if (a < b)
            System.out.println("Меньшее из двух чисел: " + a);
        else
            System.out.println("Меньшее из двух чисел: " + b);

        // Задание 2. Даны 2 числа, вывести их сумму
        int sum = a + b;
        System.out.println("Сумма двух чисел: " + sum);

        //  Задание 3. Даны 3 числа разного типа (целочисленные и с плавающей точкой), вывести их произведение
        double prod = a * b * q;
        System.out.println("Произведение чисел разных типов: " + prod);

        // Задание 4. Даны 2 числа, вывести остаток от деления этих чисел
        int c = a % b;
        System.out.println("Остаток от деления двух чисел: " + c);

        // Задение 6. Даны 2 числа с плавающей точкой, преобразовать их сумму к целочисленному значению
        System.out.println("Наши заданные 2 числа типа double: " + q + " и " + w);

        int intSum = (int)(q + w);
        System.out.println("Преобразованная к типу int сумма двух чисел: " + intSum);

    }
}
