package lesson8Homework;

public enum Months {
    JAN(1, "January"),
    FEB(2, "February"),
    MAR(3, "March"),
    APR(4, "April"),
    MAY(5, "May"),
    JUN(6, "June"),
    JUL(7, "July"),
    AUG(8, "August"),
    SEP(9, "September"),
    OCT(10, "October"),
    NOV(11, "November"),
    DEC(12, "December");

    String monthValue;
    int numberOfMonth;

    Months(int inputNumberOfMonth, String inputMonthValue){
        numberOfMonth = inputNumberOfMonth;
        monthValue = inputMonthValue;
    }

    public static String getValueOfMonthByNumber(int parameterFromScanner) {
        Months[] months = Months.values();
        for(Months month : months) {
            if(month.numberOfMonth == parameterFromScanner) {
                return month.monthValue;
            }
        }
        return null;
    }
}
