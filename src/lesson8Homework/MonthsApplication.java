package lesson8Homework;

import java.util.Scanner;

import static lesson8Homework.Months.getValueOfMonthByNumber;

public class MonthsApplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputParam = scanner.nextInt();
        System.out.println(Months.getValueOfMonthByNumber(inputParam));
    }
}
